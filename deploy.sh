#! /bin/bash

# Find and stop the existing npm start process using the PID saved in pidfile.txt
if [ -f pidfile.txt ]; then
  PID=$(cat pidfile.txt)
  kill $PID
  rm pidfile.txt
else
  echo "pidfile.txt not found. No process to stop."
fi

# Run npm start in the background and save the new PID to pidfile.txt
npm start > /dev/null 2>&1 & 
PID=$!
echo "Started npm start with PID: $PID"
echo $PID > pidfile.txt

# Add any other necessary commands

# Exit script successfully
exit 0